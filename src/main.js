import Vue from 'vue'
import App from './App.vue'
import store from './store/store.js'

new Vue({
  el: '#app',
  store,
  data: {
    nama : "messages"
  },
  render: h => h(App)
})

new Vue({
  el: '#coba',
  data: {
    nama : {
      depan : "Aji",
      belakang : "Pratama Yang Paling Ganteng"
    },

    alamat : {
      desa : "Talun ",
      kelurahan: "Bulu",
      kecamatan: "Polokarto Nun Jauh Disana",
      kabupaten: "Sukoharjo"
    }
  }
})
