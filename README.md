# vuetodo
> This is Learning VueJS by make Todo List App

## Build Setup

Sebelum memulai project mari persiapkan environment terlebih dulu
``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```
For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).
